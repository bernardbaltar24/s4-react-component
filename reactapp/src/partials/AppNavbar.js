import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';

const AppNavbar = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div className = "mb-5">
      <Navbar color="light" light expand="md" className="bg-dark">
        <NavbarBrand className="font-weight-bold text-white" href="/">MERN Tracker</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              {/*<NavLink href="" className="font-weight-bold">Members</NavLink>*/}
              <Link to="/members" className="nav-link">Members</Link>
            </NavItem>
            <NavItem>
              {/*<NavLink href="" className="font-weight-bold">Teams</NavLink>*/}
              <Link to="/teams" className="nav-link">Teams</Link>
            </NavItem>
            <NavItem>
              {/*<NavLink href="" className="font-weight-bold">Tasks</NavLink>*/}
              <Link to="/tasks" className="nav-link">Tasks</Link>
            </NavItem>
          </Nav>
          <Link to="/login" className="btn btn-primary">Login</Link>
          <Link to="/profile" className="btn btn-outline">Profile</Link>
          <button className="btn btn-secondary ml-1 font-weight-bold">Logout</button>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default AppNavbar;