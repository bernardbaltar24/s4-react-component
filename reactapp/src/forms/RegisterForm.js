import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';

const RegisterForm = (props) => {

	//STATE
	//object that holds a component's dynamic data
	//useState
	// const [username, setUsername] = useState("test")
	// console.log(username)
  // const [email, setEmail] = useState("test")
  // const [password, setPassword] = useState("test")
  // const [password2, setPassword2] = useState("test")

  const[formData, setFormData] = useState({
    username: "",
    email: "",
    password: "",
    password2: ""
  })

  const [disabledBtn, setDisabledBtn] = useState(true)

  const [isRedirected, setIsRedirected] = useState(false)

  const {username, email, password, password2} = formData;

  const onChangeHandler = e => {
    // console.log(e);
    // console.log(e.target);

    //Backticks - multi-line and string interpolation
    // console.log(`e.target.name : ${e.target.name} `)
    // console.log(`e.target.value : ${e.target.value} `)

    //update the state
    setFormData({
      ...formData,
      [e.target.name] : e.target.value
    })
  }

  const onSubmitHandler = async e => {
    e.preventDefault();
    //check if password match
    if(password!==password2){
      console.log("Password don't match")
      Swal.fire({
        title: "Error!",
        text: "Passwords don't match!",
        icon: "error",
        showConfirmationButton: false,
        timer: 1500
      })
    }else{
      console.log(formData)

      //create a member object
      const newMember = {
        username,
        email,
        password
      }

      try{
      //create config
      const config = {
      headers: {
        'Content-Type': 'application/json'
      }
      }
      //create the body
      const body = JSON.stringify(newMember) //converting newMbember into a string
      //access route using axios
      const res = await axios.post("http://localhost:5000/members", body, config)
      console.log(res)

      //redirect
      // <Redirect to="/login" />
      setIsRedirected(true)
      }catch(e){
        console.log(e)
      }
      
    }
  }

  //USE EFFECT
  //Hook that tells the component what to do after render
  useEffect(()=>{
  if(username !== "" && email !== "" && password !== "" && password2 !== "" ){
    setDisabledBtn(false) 
  }else{
    setDisabledBtn(true)
  }
  }, [formData])

if(isRedirected){
  return <Redirect to="/login" />
}

  return (
    <Form onSubmit={ e => onSubmitHandler(e)}>
      <FormGroup>
        <Label for="username" className="mt-3">Username</Label>
        <Input 
        type="text" 
        name="username" 
        id="username" 
        placeholder="Please input username"
        value={username}
        onChange={e => onChangeHandler(e)}
        // onChange={e => setUsername(e.target.value)}
        maxLength="30"
        pattern="[a-zA-Z0-9]+"
        required
         />
      </FormGroup>

      <FormText>Use alphanumeric characters only</FormText>

      <FormGroup>
        <Label for="email" className="mt-3">Email</Label>
        <Input 
        type="email" 
        name="email" 
        id="email" 
        placeholder="Please input correct Email"
        value={email}
        onChange={e => onChangeHandler(e)}
        // onChange={e => setEmail(e.target.value)}
        required
         />
      </FormGroup>

      <FormText>Use alphanumeric characters only</FormText>

      <FormGroup>
        <Label for="password">Password</Label>
        <Input 
        type="password" 
        name="password" 
        id="password" 
        placeholder="Please input Password"
        value={password}
        onChange={e => onChangeHandler(e)}
        // onChange={e => setPassword(e.target.value)}
        required
        minLength="5"
        />
      </FormGroup>

      <FormGroup>
        <Label for="password2">Verify Password</Label>
        <Input 
        type="password" 
        name="password2" 
        id="password2" 
        placeholder="Please input Password"
        value={password2}
        onChange={e => onChangeHandler(e)}
        // onChange={e => setPassword2(e.target.value)}
        required
        minLength="5"
        />
      </FormGroup>

     <Button 
      className="mb-3 btn btn-block"
      disabled={disabledBtn}
      >Submit</Button>
      <p>
      Already have an account? <Link to="/login">Login</Link>.
      </p>
    </Form>
  );
}

export default RegisterForm;