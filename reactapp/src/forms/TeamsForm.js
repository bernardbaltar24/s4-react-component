import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link } from 'react-router-dom';

const TeamsForm = (props) => {
	const[formData, setFormData] = useState({
    teamname: ""
  })

  const [disabledBtn, setDisabledBtn] = useState(true)

  const {teamname, password} = formData;

  const onChangeHandler = e => {

    setFormData({
      ...formData,
      [e.target.name] : e.target.value
    })
  }

const onSubmitHandler = e => {
    e.preventDefault();
    console.log(formData)
  }

  useEffect(()=>{
  if(teamname !== ""){
    setDisabledBtn(false) 
  }else{
    setDisabledBtn(true)
  }
  }, [formData])

  return (
    <Form onSubmit={ e => onSubmitHandler(e)}>
      <FormGroup>
        <Label for="teamname" className="mt-3">Team Name</Label>
        <Input 
        type="text" 
        name="teamname" 
        id="teamname" 
        placeholder="Please input Team Name"
        value={teamname}
        onChange={e => onChangeHandler(e)}
        // onChange={e => setteamname(e.target.value)}
        maxLength="30"
        pattern="[a-zA-Z0-9]+"
        required
         />
      </FormGroup>

      <Button 
      className="mb-3 btn btn-block"
      disabled={disabledBtn}
      >Login</Button>
    </Form>
  );
}

export default TeamsForm;