import React from 'react';
import { Table } from 'reactstrap';
import TasksTableBody from '../rows/TasksTableBody';

const TaskTables = (props) => {
  return (
    <Table>
      <thead>
        <tr>
          <th>#</th>
          <th>Description</th>
          <th>Member ID</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <TasksTableBody/>
      </tbody>
    </Table>
  );
}

export default TaskTables;