import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import {Redirect, BrowserRouter, Route, Switch } from 'react-router-dom';
// import { Button } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.css";

//pages
import AppNavbar from './partials/AppNavbar' //kahit walang extension kasi js din sya.
import MembersPage from './pages/MembersPage';
import TeamsPage from './pages/TeamsPage';
import TasksPage from './pages/TasksPage';
import NotFoundPage from './pages/NotFoundPage';
import LogInPage from './pages/LogInPage';
import LandingPage from './pages/LandingPage';
import ProfilePage from './pages/ProfilePage';
import RegisterPage from './pages/RegisterPage';



const root = document.querySelector("#root")

// const pageComponent = (
// 	<Fragment>
// 	<h1 className="header"> Hello, Batch 40! </h1>
// 	<button className="button"> Pink </button>
// 	<button className="btn btn-danger"> Danger </button>

// 	<Button color="primary" className="ml-3"> Reactstrap Button </Button>

// {/* how to comment out*/}
// 	</Fragment>
// 	)

const pageComponent = (
	<BrowserRouter>
		<AppNavbar/>
		<Switch>	
			<Route component= {MembersPage} path="/members"/>
			<Route component= {TeamsPage} path="/teams"/>
			<Route component= {TasksPage} path="/tasks"/>
			<Route component= {NotFoundPage} path="/error"/>
			<Route component= {LogInPage} path="/login"/>
			<Route component= {RegisterPage} path="/register"/>
			<Route component= {ProfilePage} path="/profile"/>
			<Route component= {LandingPage} exact path="/"/>
			<Redirect to="/error"/>
		</Switch>
	</BrowserRouter>)

ReactDOM.render(pageComponent, root);